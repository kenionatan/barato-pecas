import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-marca',
  templateUrl: 'marca.html',
})
export class MarcaPage {
  selectedItem: any;
  data:any = {};
  idItem:any;
  produtos : any[];

  constructor(public navCtrl: NavController, public http : HttpClient, public navParams: NavParams) {
    this.data.response = '';
    this.http = http;
    this.selectedItem = navParams.get('user');
    this.submit();
    //console.log(this.selectedItem.id);
  }

  submit() {
    var link = 'http://www.kenio.top/api_baratopecas/apiListaProdMontadoras.php';
    var myData = JSON.stringify(this.selectedItem.id);
    
    this.data = this.http.post(link, myData);
    this.data.subscribe(data => {
    this.produtos = data;
    //this.data.response = data["_body"];
    console.log(data);
    }, error => {
    console.log("Oooops!");
    });
  }



}
