import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { NgForm } from '@angular/forms';
import { Http, Headers } from '@angular/http';

@IonicPage()
@Component({
  selector: 'page-busca',
  templateUrl: 'busca.html',
})
export class BuscaPage {
  produtos : any[];
  api_sefaz:string = "http://api.sefaz.al.gov.br/sfz_nfce_api/api/public/consultarPrecosPorDescricao";
  value: any;

  constructor(public navCtrl: NavController, public service : ServiceProvider, public navParams: NavParams, public loadingCtrl: LoadingController, public httpp: Http) {
    //this.value = navParams.get('busca');
    //console.log(this.value);
    this.postDados();
  }

  postDados(){
    //this.service.postData()
    this.postData()
    .subscribe(data => {
      this.produtos = data.json();
      console.log(data);
    });
  }

  postData(){

    //console.log(this.navParams.get("busca"));

    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("AppToken", "b26dadf220c020a0f03dc55e00874cefe6e67191");
    

    let body = {
      descricao: this.navParams.get("busca"), //"coca cola",
      dias: 3,
      latitude: -9.6432331,
      longitude: -35.7190686,
      raio: 15
    };

    return this.httpp.post(this.api_sefaz, JSON.stringify(body), {headers: headers});

  }



}
