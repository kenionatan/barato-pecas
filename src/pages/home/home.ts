import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { MarcaPage } from '../marca/marca';
import { CategoriasPage } from '../categorias/categorias';
import { ProdutoscategoriasPage } from '../produtoscategorias/produtoscategorias';
import { BuscaPage } from '../busca/busca';
import { FormBuilder, Validators } from '@angular/forms';
import 'rxjs/Rx'; import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  users : any[];
  brands : any[];
  categorias : any[];
  busca : any = {};

  constructor(public navCtrl: NavController, public service : ServiceProvider, public loadingCtrl: LoadingController, public formBuilder: FormBuilder) {
    this.busca = this.formBuilder.group({
      texto:['', Validators.required],
    });
    this.getDados();
    this.getBrands();
    this.getCategorias();
  }

  getDados(){
    this.service.getData().subscribe(
      (data: any[]) => this.users = data,
      err => console.log(err)
    );
  }

  getCategorias(){
    this.service.getDataCategorias().subscribe(
      (data: any[]) => this.categorias = data,
      err => console.log(err)
    );
  }

  getBrands(){
    this.service.getDataBrands().subscribe(
      (data: any[]) => this.brands = data,
      err => console.log(err)
    );
  }

  openPage(event, user) {
    this.navCtrl.push(MarcaPage, {
      user: user
    });
  }

  openCategoria(event, categoria) {
    this.navCtrl.push(ProdutoscategoriasPage, {
      categoria: categoria
    });
    //console.log(categoria);
  }

  buscar(busca){
    console.log(this.busca.value.texto);
    this.navCtrl.push(BuscaPage,{
      busca:this.busca.value.texto
    });
  }

  

}
