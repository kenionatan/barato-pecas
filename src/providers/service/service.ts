import { HttpClient } from '@angular/common/http';
import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

/*
  Generated class for the ServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServiceProvider {

  api:string = "http://www.kenio.top/api_baratopecas/";
  api_sefaz:string = "http://api.sefaz.al.gov.br/sfz_nfce_api/api/public/consultarPrecosPorDescricao";

  constructor(public http: HttpClient, public httpp: Http) {
    
  }

  getData(){
    return this.http.get(this.api+'apiListaMontadoras.php');
  }

  getDataBrands(){
    return this.http.get(this.api+'apiListaBrands.php');
  }

  getDataCategorias(){
    return this.http.get(this.api+'apiListaCategorias.php');
  }

  postData(){

    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("AppToken", "b26dadf220c020a0f03dc55e00874cefe6e67191");

    let body = {
      descricao: "coca cola",
      dias: 3,
      latitude: -9.6432331,
      longitude: -35.7190686,
      raio: 15
    };

    return this.httpp.post(this.api_sefaz, JSON.stringify(body), {headers: headers});

  }


  

}
